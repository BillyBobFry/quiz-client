import { createMuiTheme } from "@material-ui/core/styles";
import purple from "@material-ui/core/colors/purple";
import { amber } from "@material-ui/core/colors/";

const palette = {
  primary: {
    main: purple[500],
  },
  secondary: {
    main: amber[500],
  },
};

const headerFamily = "'Grandstander', sans-serif";

export const DefaultTheme = createMuiTheme({
  palette,
  typography: {
    allVariants: {
      fontFamily: "'Grandstander', sans-serif",
    },
    h1: {
      fontFamily: headerFamily,
      fontSize: "5rem",
      fontWeight: 600,
      textShadow: `5px 5px #00000066`,
    },
    h2: {
      fontFamily: headerFamily,
      fontSize: "2.5rem",
      fontWeight: 400,
      textShadow: `3px 3px #00000066`,
    },
    h3: {
      fontFamily: headerFamily,
    },
    h4: {
      fontFamily: headerFamily,
      textShadow: "2px 2px #00000088",
    },
    h5: {
      fontFamily: headerFamily,
      color: "rgba(0,0,0,0.75)",
      textShadow: "1px 1px #00000088",
      fontWeight: 600,
      fontStyle: "italic",
    },
    h6: {
      fontFamily: headerFamily,
      textShadow: "1px 1px #00000088",
    },
  },
  props: {
    MuiButton: {
      disableElevation: true,
    },
  },
  overrides: {
    MuiButton: {
      root: {
        fontFamily: headerFamily,
        "&.Mui-disabled": { backgroundColor: "#ffffff88!important" },
      },
    },
    MuiCard: {
      root: {
        width: "90%",
        maxWidth: "800px",
        backgroundColor: "#ffffffee",
        border: `5px solid ${palette.secondary.main}`,
        borderRadius: 6,
      },
    },
    MuiDialog: {
      paper: {
        border: `5px solid ${palette.secondary.main}`,
      },
    },
    MuiList: {
      root: {},
    },
    MuiListItem: {
      root: {
        "&:not(:last-child)": { borderBottom: `1px solid grey` },
      },
    },
    MuiTable: {
      root: {
        backgroundColor: "#ffffffdd",
      },
    },
    MuiTableHead: {
      root: {
        fontFamily: headerFamily,
      },
    },
  },
});
