import React, { createContext, useReducer, useMemo, useContext } from "react";
import {
  UnstartedGame,
  GameContextState,
  gameContextReducer,
  GameAction,
} from ".";
import { useHistory, useLocation } from "react-router-dom";
import { errorStore } from "..";
import { subscribeToGame } from "../../integration";
const dispatch: React.Dispatch<GameAction> = (value: GameAction) => {};

const initialState: UnstartedGame = {
  id: "",
  players: [],
  currentPlayer: localStorage.getItem("playerName") || null,
  started: false,
  roundLimit: 10,
  rounds: [],
  round: "unstarted",
};

export const store = createContext<{
  state: GameContextState;
  dispatch: React.Dispatch<GameAction>;
  ws?: WebSocket;
}>({ state: initialState, dispatch });
const { Provider } = store;

export const GameStateProvider = ({ children }: { children: any }) => {
  const l = useLocation();
  const gameID = l.pathname.slice(1);
  const { push } = useHistory();
  const [state, dispatch] = useReducer(gameContextReducer, {
    id: "",
    players: [],
    currentPlayer: localStorage.getItem(`playerName-${gameID}`) || null,
    started: false,
    roundLimit: 10,
    rounds: [],
    round: "unstarted",
  });
  console.log(localStorage.getItem("playerName"));

  const [, errorDispatch] = useContext(errorStore);

  const ws = useMemo(
    () => new WebSocket(`wss://${process.env.REACT_APP_API_HOST}`),
    []
  );

  ws.onopen = () => {
    if (gameID) {
      subscribeToGame(ws, gameID);
      dispatch({ type: "join lobby", gameID });
    }
  };

  ws.onmessage = (message: MessageEvent<string>) => {
    console.log(message);
    const data = JSON.parse(message.data);
    const { type } = data;
    switch (type) {
      case "create game":
        dispatch({ type: "initialise game", id: data.id });
        push(`/${data.id}`);
        break;
      case "add player":
        dispatch({ type: "add player", playerName: data.playerName });
        break;
      case "load game":
        if (data.game) {
          dispatch({ type: "load game", game: data.game });
        } else {
          errorDispatch({
            type: "set error",
            message: `Game${(gameID || "") + " "}not found, try again.`,
          });
          push("/");
        }
        break;
      case "set ready":
        dispatch({
          type: "set ready",
          playerName: data.playerName,
          ready: data.ready,
        });
        break;
      case "set options":
        dispatch({
          type: "set options",
          currentChooser: data.currentChooser,
          questionOptions: data.questionOptions,
        });
        break;
      case "set round limit":
      case "set question":
      case "show answers":
      case "finish round":
      case "game over":
        dispatch(data);
        break;
      default:
        break;
    }
  };

  return <Provider value={{ state, dispatch, ws }}>{children}</Provider>;
};
