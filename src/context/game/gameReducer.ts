import { GameContextState, GameAction, Player } from ".";

export const gameContextReducer = (
  state: GameContextState,
  action: GameAction
): GameContextState => {
  switch (action.type) {
    case "initialise game":
      return {
        id: action.id,
        players: [],
        currentPlayer: null,
        started: false,
        roundLimit: 10,
        rounds: [],
        round: "unstarted",
      };
    case "set round limit":
      return {
        ...state,
        roundLimit: action.roundLimit,
      };
    case "add player":
      const newPlayers: Player[] = [
        ...state.players,
        { name: action.playerName, points: 0, ready: false },
      ];
      return { ...state, players: newPlayers };
    case "load game":
      return { ...state, ...action.game };
    case "join game":
      return { ...state, currentPlayer: action.playerName };
    case "join lobby":
      return { ...state, id: action.gameID };
    case "set ready":
      const srPlayers: Player[] = state.players.map(player =>
        player.name === action.playerName
          ? { ...player, ready: action.ready }
          : player
      );
      return { ...state, players: srPlayers };
    case "set options":
      return {
        ...state,
        round: "choosing",
        currentChooser: action.currentChooser,
        questionOptions: action.questionOptions,
        started: true,
      };
    case "set question":
      return {
        ...state,
        round: "answering",
        currentQuestion: action.currentQuestion,
        started: true,
      };
    case "finish round":
      return {
        ...state,
        started: true,
        round: "show answers",
        rounds: [...state.rounds, action.round],
      };
    case "game over":
      return { ...state, round: "game over", started: true };
    default:
      throw new Error();
  }
};
