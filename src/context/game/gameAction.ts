import { GameContextState, Question, Round } from ".";

type SetReady = { type: "set ready"; ready: boolean; playerName: string };

type SetRoundLimit = { type: "set round limit"; roundLimit: number };

type AddPlayer = { type: "add player"; playerName: string };

type InitialiseGame = { type: "initialise game"; id: string };

type LoadGame = { type: "load game"; game: GameContextState };

type JoinGame = { type: "join game"; playerName: string };

type JoinLobby = { type: "join lobby"; gameID: string };

type SetOptions = {
  type: "set options";
  questionOptions: Question[];
  currentChooser: string;
};

type ShowAnswers = {
  type: "show answers";
  currentQuestion: string;
  correctAnswer: string;
  chosenAnswers: { player: string; answer: string; points: number }[];
};

type FinishRound = {
  type: "finish round";
  round: Round;
};

type SetQuestion = {
  type: "set question";
  currentQuestion: Question;
};

type GameOver = {
  type: "game over";
};
export type GameAction =
  | SetRoundLimit
  | AddPlayer
  | InitialiseGame
  | LoadGame
  | JoinGame
  | JoinLobby
  | SetReady
  | SetOptions
  | SetQuestion
  | FinishRound
  | ShowAnswers
  | GameOver;
