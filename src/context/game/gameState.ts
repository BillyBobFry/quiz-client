export type Player = { name: string; points: number; ready: boolean };

type BaseGame = {
  id: string;
  players: Player[];
  currentPlayer: string | null;
  roundLimit: number;
  rounds: Round[];
};

type BaseQuestion = {
  category: string;
  question: string;
  correctAnswer: string;
};

export type MultipleChoiceQuestion = BaseQuestion & {
  type: "Multiple Choice";
  incorrectAnswers: string[];
};

export type ClosestQuestion = BaseQuestion & {
  type: "Closest";
};

export type Question = MultipleChoiceQuestion | ClosestQuestion;

export type Round = {
  question: Question;
  questionChooser: string;
  /**
   * [playerName, answer, points]
   */
  points: {
    playerName: string;
    answer: string;
    correct?: boolean;
    points?: number;
  }[];
};

export type UnstartedGame = BaseGame & { started: false; round: "unstarted" };

export type StartedGame = BaseGame & { started: true };

export type Choosing = StartedGame & {
  round: "choosing";
  currentChooser: string;
  questionOptions: Question[];
};
export type Answering = StartedGame & {
  round: "answering";
  currentQuestion: Question;
};

export type ShowAnswers = StartedGame & {
  round: "show answers";
};

export type GameOver = StartedGame & {
  round: "game over";
};

export type GameContextState =
  | Choosing
  | Answering
  | ShowAnswers
  | UnstartedGame
  | GameOver;
