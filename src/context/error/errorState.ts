export type ErrorContextState = {
  message?: string;
  title?: string;
};
