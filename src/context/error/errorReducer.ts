import { ErrorAction, ErrorContextState } from ".";

export const errorContextReducer = (
  state: ErrorContextState,
  action: ErrorAction
): ErrorContextState => {
  switch (action.type) {
    case "set error":
      return { ...state, message: action.message, title: action.title };
    case "clear error":
      return {};
  }
};
