type SetError = { type: "set error"; message: string; title?: string };
type ClearError = { type: "clear error" };

export type ErrorAction = SetError | ClearError;
