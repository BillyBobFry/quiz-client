import React, { createContext, useReducer } from "react";
import { ErrorAction, ErrorContextState, errorContextReducer } from ".";

const dispatch: React.Dispatch<ErrorAction> = (value: ErrorAction) => {};

const initialState: ErrorContextState = {};

export const errorStore = createContext<
  [ErrorContextState, React.Dispatch<ErrorAction>]
>([initialState, dispatch]);

const { Provider } = errorStore;

export const ErrorStateProvider = ({ children }: { children: any }) => {
  const [state, dispatch] = useReducer(errorContextReducer, initialState);

  return <Provider value={[state, dispatch]}>{children}</Provider>;
};
