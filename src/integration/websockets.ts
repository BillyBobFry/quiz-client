import { GameAction, Question } from "../context";

export const setReady = (
  ws: WebSocket,
  gameID: string,
  playerName: string,
  ready: boolean
) => {
  broadcastAction(ws, gameID, { type: "set ready", playerName, ready });
};

export const addPlayerToGame = (
  ws: WebSocket,
  gameID: string,
  playerName: string
) => {
  broadcastAction(ws, gameID, { type: "add player", playerName });
};

export const subscribeToGame = (ws: WebSocket, gameID: string) => {
  ws.send(JSON.stringify({ gameID, action: { type: "subscribe" } }));
};

export const startGame = (ws: WebSocket, gameID: string) => {
  ws.send(JSON.stringify({ gameID, action: { type: "start game" } }));
};

export const setRoundLimit = (
  ws: WebSocket,
  gameID: string,
  roundLimit: number
) => {
  broadcastAction(ws, gameID, { type: "set round limit", roundLimit });
};

export const chooseQuestion = (
  ws: WebSocket,
  gameID: string,
  question: Question,
  questionChooser: string
) => {
  ws.send(
    JSON.stringify({
      gameID,
      action: { type: "choose question", question, questionChooser },
    })
  );
};

/**
 * Choose an answer to the current question
 * @param ws
 * @param gameID
 * @param playerName
 * @param answer
 */
export const chooseAnswer = (
  ws: WebSocket,
  gameID: string,
  playerName: string,
  answer: string
) => {
  ws.send(
    JSON.stringify({
      gameID,
      action: { type: "choose answer", playerName, answer },
    })
  );
};

/**
 * Broadcast an action to all other clients connected to this game
 * @param ws - the websocket connection
 * @param gameID - the game id to which this action relates
 * @param action - the action to broadcast to all connected clients
 */
export const broadcastAction = (
  ws: WebSocket,
  gameID: string,
  action: GameAction
) => {
  ws.send(JSON.stringify({ gameID, action }));
};
