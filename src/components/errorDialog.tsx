import React, { useContext } from "react";
import { errorStore } from "../context";
import {
  Dialog,
  Typography,
  makeStyles,
  DialogActions,
  Button,
} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  errorDialog__paper: {
    padding: `${theme.spacing(3)}px ${theme.spacing(3)}px 0 ${theme.spacing(
      3
    )}px`,
    "& .errorDialog__actions": {
      marginTop: theme.spacing(1),
    },
  },
}));

export const ErrorDialog = () => {
  const classes = useStyles();
  const [state, dispatch] = useContext(errorStore);
  const { message, title } = state;

  const clearError = () => {
    dispatch({ type: "clear error" });
  };

  return Boolean(message || title) ? (
    <Dialog open PaperProps={{ className: classes.errorDialog__paper }}>
      <Typography>{message}</Typography>
      <DialogActions className="errorDialog__actions">
        <Button
          variant="contained"
          color="primary"
          disableElevation
          onClick={clearError}
        >
          OK
        </Button>
      </DialogActions>
    </Dialog>
  ) : null;
};
