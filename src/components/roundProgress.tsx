import React, { useContext, useMemo } from "react";
import { Box, Typography } from "@material-ui/core";
import { store } from "../context";

export const RoundProgress = () => {
  const { state } = useContext(store);
  const { roundLimit, rounds, round } = state;

  const roundsSoFar = useMemo(() => rounds.length + 1, [rounds]);

  return (
    <Box>
      <Typography color="secondary" variant="h4">
        {round === "game over"
          ? "Final Scores"
          : `Round ${
              round === "show answers" ? roundsSoFar - 1 : roundsSoFar
            } of 
        ${roundLimit}`}
      </Typography>
    </Box>
  );
};
