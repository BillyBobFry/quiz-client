import React, { useContext, useEffect, useState } from "react";
import { Box, Slider, Typography } from "@material-ui/core";
import { store } from "../context";
import { setRoundLimit } from "../integration";
import { SliderProps } from "material-ui";

export type RoundLimitProps = Omit<
  SliderProps,
  "value" | "onChange" | "min" | "max" | "onChangeCommitted"
>;

export const RoundLimit = (props: RoundLimitProps) => {
  const { state, ws } = useContext(store);
  const { roundLimit, id } = state;
  const [value, setValue] = useState(roundLimit);

  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    if (roundLimit !== value) setValue(roundLimit);
  }, [roundLimit]);
  /* eslint-enable react-hooks/exhaustive-deps */

  return (
    <Box>
      <Typography color={"primary"}>{value} Rounds</Typography>
      <Slider
        color="primary"
        min={5}
        max={50}
        value={value}
        onChange={(e, val) => {
          setValue(val as number);
        }}
        onChangeCommitted={(e, val) => {
          setRoundLimit(ws!, id, val as number);
        }}
        {...props}
      />
    </Box>
  );
};
