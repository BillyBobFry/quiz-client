import { List, ListItem, Box, Typography, makeStyles } from "@material-ui/core";
import React, { useContext } from "react";
import { store, Choosing, Question } from "../context";
import { chooseQuestion } from "../integration";
import {
  Public as GlobeIcon,
  Movie as MovieIcon,
  MusicNote as MusicIcon,
  MenuBook as BookIcon,
  History as HistoryIcon,
  SportsCricket as SportIcon,
  SupervisorAccount as CultureIcon,
  EmojiObjects as ScienceIcon,
  School as GeneralKnowledgeIcon,
  Restaurant as FoodIcon,
} from "@material-ui/icons";

const IconMap: Map<Question["category"], React.ReactElement> = new Map();
IconMap.set("Art and Literature", <BookIcon />);
IconMap.set("Geography", <GlobeIcon />);
IconMap.set("General Knowledge", <GeneralKnowledgeIcon />);
IconMap.set("History", <HistoryIcon />);
IconMap.set("Film and TV", <MovieIcon />);
IconMap.set("Food and Drink", <FoodIcon />);
IconMap.set("Music", <MusicIcon />);
IconMap.set("Science", <ScienceIcon />);
IconMap.set("Society and Culture", <CultureIcon />);
IconMap.set("Sport and Leisure", <SportIcon />);

const useStyles = makeStyles(theme => ({
  questionOptions: {
    "& .questionOptions__option": {
      display: "grid",
      gridTemplateColumns: "10% 90%",
    },
  },
}));

export const QuestionOptions = () => {
  const classes = useStyles();
  const { state, ws } = useContext(store);
  const { id, questionOptions, currentPlayer } = state as Choosing;

  return (
    <Box className={classes.questionOptions}>
      <Typography variant="h5" color="primary" align="center">
        Choose a question:
      </Typography>
      <List>
        {questionOptions.map(question => (
          <ListItem
            key={question.question}
            className="questionOptions__option"
            button
            onClick={() => {
              chooseQuestion(ws!, id, question, currentPlayer!);
            }}
          >
            <span>{IconMap.get(question.category)}</span>
            <span>{question.question}</span>
          </ListItem>
        ))}
      </List>
    </Box>
  );
};
