import React, { HTMLAttributes } from "react";
import {
  makeStyles,
  Box,
  Card as MuiCard,
  Typography,
} from "@material-ui/core";
export interface CardProps extends HTMLAttributes<HTMLDivElement> {
  title?: string;
}
const useStyles = makeStyles((theme) => ({
  card: {
    padding: theme.spacing(4),
  },
}));

export const Card = (props: CardProps) => {
  const { children, title } = props;
  const classes = useStyles();
  return (
    <Box {...props}>
      {title && (
        <Typography variant="h2" color="secondary" align="center">
          {title}
        </Typography>
      )}
      <MuiCard style={{ margin: "auto" }} className={classes.card}>
        {children}
      </MuiCard>
    </Box>
  );
};
