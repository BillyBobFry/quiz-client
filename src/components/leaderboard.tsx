import { Box, List, ListItem, ListItemText } from "@material-ui/core";
import React, { HTMLAttributes } from "react";
import { usePlayerPoints } from "../hooks";

export const Leaderboard = (props: HTMLAttributes<HTMLDivElement>) => {
  const { descendingScore } = usePlayerPoints();

  return (
    <Box {...props}>
      <List>
        {descendingScore.map(player => (
          <ListItem key={player.name}>
            <ListItemText>
              {player.name} - {player.points}
            </ListItemText>
          </ListItem>
        ))}
      </List>
    </Box>
  );
};
