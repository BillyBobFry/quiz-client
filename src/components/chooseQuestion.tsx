import { Card, makeStyles, Typography } from "@material-ui/core";
import React, { useContext } from "react";
import { Choosing, store } from "../context";
import { QuestionOptions } from "./questionOptions";

const useStyles = makeStyles(theme => ({
  chooseQuestion: {
    padding: theme.spacing(2),

    "& .chooseQuestion__currentQuestion": {
      padding: theme.spacing(3),
    },
  },
}));

export const ChooseQuestion = () => {
  const classes = useStyles();
  const { state } = useContext(store);
  const { currentPlayer, currentChooser } = state as Choosing;

  return (
    <Card className={classes.chooseQuestion}>
      {currentPlayer === currentChooser ? (
        <QuestionOptions />
      ) : (
        <Typography color="primary" className="chooseQuestion__currentQuestion">
          {currentChooser} is choosing a question.
        </Typography>
      )}
    </Card>
  );
};
