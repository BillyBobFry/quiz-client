import { Typography, makeStyles, Card } from "@material-ui/core";
import React, { useContext } from "react";
import { Answering, store } from "../context";
import { ChooseMultipleChoiceAnswer, ChooseClosestAnswer } from "./";

const useStyles = makeStyles(theme => ({
  chooseAnswer: {
    "& .chooseAnswer__question": {
      padding: theme.spacing(2),
    },
  },
}));

export const ChooseAnswer = () => {
  const classes = useStyles();
  const { state } = useContext(store);
  const { currentQuestion } = state as Answering;
  return (
    <Card className={classes.chooseAnswer}>
      <Typography
        color="primary"
        className="chooseAnswer__question"
        variant="h5"
      >
        {currentQuestion.question}
      </Typography>
      {currentQuestion.type === "Multiple Choice" ? (
        <ChooseMultipleChoiceAnswer {...currentQuestion} />
      ) : (
        <ChooseClosestAnswer {...currentQuestion} />
      )}
    </Card>
  );
};
