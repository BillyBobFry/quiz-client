import { Box, Button, TextField } from "@material-ui/core";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";

export const JoinGameForm = () => {
  const [gameID, setGameID] = useState<string>("");

  const { push } = useHistory();

  const _joinGame = () => {
    push(`/${gameID}`);
  };

  return (
    <Box>
      <TextField
        label="Game ID"
        value={gameID}
        onChange={e => setGameID(e.target.value)}
        inputRef={input => input && input.focus()}
      />
      <Button
        onClick={_joinGame}
        variant="contained"
        color="primary"
        disabled={!gameID}
      >
        Join!
      </Button>
    </Box>
  );
};
