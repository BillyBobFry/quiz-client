import React, { useContext } from "react";
import { Button, makeStyles, Card, Box, Typography } from "@material-ui/core";
import { JoinGameForm } from "../components";
import { store } from "../context";
const useStyles = makeStyles((theme) => ({
  startPage: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-evenly",
    alignItems: "center",
    height: "100%",

    "& .startPage__card": {
      display: "flex",
      flexDirection: "column",
      justifyContent: "space-evenly",
      alignItems: "center",
      backgroundColor: "#ffffffdd",
      "& button": {
        textTransform: "none",
      },
      padding: theme.spacing(3),
      gap: theme.spacing(1),
    },
    "& .startPage__gameIDInput": {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
  },
}));

export const StartPage = () => {
  const classes = useStyles();
  const { ws } = useContext(store);

  const createGame = () => {
    if (ws) ws.send(JSON.stringify({ action: { type: "create game" } }));
  };
  return (
    <Box className={classes.startPage}>
      <Card className="startPage__card">
        <Button style={{ height: "100%", width: "100%" }} onClick={createGame}>
          <Typography color="primary" variant="h5">
            Create a Game
          </Typography>
        </Button>
      </Card>
      <Card className="startPage__card">
        <Typography color="primary" variant="h5">
          Join a game
        </Typography>
        <JoinGameForm />
      </Card>
    </Box>
  );
};
