import {
  Box,
  Button,
  TextField,
  makeStyles,
  Typography,
} from "@material-ui/core";
import React, { useContext, useState } from "react";
import { store } from "../context";
import { addPlayerToGame } from "../integration";

const useStyles = makeStyles((theme) => ({
  createPlayerForm: {
    display: "flex",
    flexDirection: "column",
    gap: theme.spacing(2),
    "& .createPlayerForm__form": {
      display: "flex",
      alignItems: "center",
      gap: theme.spacing(2),
    },
  },
}));

export const CreatePlayerForm = () => {
  const classes = useStyles();
  const [playerName, setPlayerName] = useState<string>("");

  const { state, ws, dispatch } = useContext(store);

  const addPlayer = () => {
    const { id } = state;
    if (ws && playerName) {
      addPlayerToGame(ws, id, playerName!);
      localStorage.setItem(`playerName-${id}`, playerName);
      dispatch({ type: "join game", playerName: playerName! });
    }
  };

  return (
    <Box className={classes.createPlayerForm}>
      <Typography color="primary" variant="h5">
        Choose a Name
      </Typography>
      <Box className="createPlayerForm__form">
        <TextField
          value={playerName}
          onChange={(e) => setPlayerName(e.target.value)}
          inputRef={(input) => input && input.focus()}
        />
        <Button
          onClick={addPlayer}
          variant="contained"
          color="primary"
          disabled={!playerName}
        >
          Join!
        </Button>
      </Box>
    </Box>
  );
};
