import React, { useContext, useEffect } from "react";
import { makeStyles, Dialog, Box, IconButton } from "@material-ui/core";
import { store } from "../context";
import { GameLobby } from "./gameLobby";
import { CreatePlayerForm } from "./createPlayerForm";
import { subscribeToGame } from "../integration";
import { useHistory, useParams } from "react-router-dom";
import { Game } from "./";
import { ArrowBack } from "@material-ui/icons";

const useStyles = makeStyles(theme => ({
  gamePage: {
    height: "100%",
    width: "100%",
    display: "flex",
    justifyContent: "space-evenly",
    alignItems: "center",

    "& .gamePage__backButton": {
      position: "fixed",
      top: 0,
      left: 0,
      margin: theme.spacing(1.5),
    },
  },
  gamePage__dialog: {
    padding: theme.spacing(2),
    display: "flex",
    alignItems: "center",
  },
}));

export const GamePage = () => {
  const classes = useStyles();
  const { dispatch, state, ws } = useContext(store);
  const { currentPlayer, started } = state;
  const { gameID } = useParams<{ gameID: string }>();
  const { push } = useHistory();

  const goHome = () => {
    push("/");
  };

  useEffect(() => {
    if (ws && ws.readyState === 1) {
      subscribeToGame(ws, gameID);
      dispatch({ type: "join lobby", gameID });
    }
  }, [gameID, dispatch, ws]);

  return (
    <Box className={classes.gamePage}>
      <IconButton onClick={goHome} className="gamePage__backButton">
        <ArrowBack color="secondary" />
      </IconButton>
      {started ? <Game /> : <GameLobby />}
      {!currentPlayer && (
        <Dialog PaperProps={{ className: classes.gamePage__dialog }} open>
          <CreatePlayerForm />
        </Dialog>
      )}
    </Box>
  );
};
