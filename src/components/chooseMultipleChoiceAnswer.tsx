import { Button, List, ListItem } from "@material-ui/core";
import React, { useContext } from "react";
import { Answering, MultipleChoiceQuestion, store } from "../context";
import { chooseAnswer } from "../integration";
import { shuffle } from "../utils/arrayUtils";

export const ChooseMultipleChoiceAnswer = (props: MultipleChoiceQuestion) => {
  const { state, ws } = useContext(store);
  const { id, currentPlayer } = state as Answering;

  const { correctAnswer, incorrectAnswers } = props;

  const answerOptions = shuffle([correctAnswer, ...incorrectAnswers]);
  return (
    <List>
      {answerOptions.map(answer => (
        <ListItem key={answer}>
          <Button
            fullWidth
            onClick={() => {
              chooseAnswer(ws!, id, currentPlayer!, answer);
            }}
          >
            {answer}
          </Button>
        </ListItem>
      ))}
    </List>
  );
};
