import { Box, Card, Typography, List, makeStyles } from "@material-ui/core";
import React, { useContext } from "react";
import { store, ShowAnswers } from "../context";
import { PlayerAnswer, Countdown } from "./";
import { Beenhere, LiveHelp } from "@material-ui/icons";
const useStyles = makeStyles(theme => ({
  showPlayerAnswers: {
    "& .showPlayerAnswers__questionAnswer": {
      display: "grid",
      gridTemplateColumns: "10% 90%",
      alignItems: "center",
      justifyContent: "center",
      margin: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
      "& > :not(:first-child)": {
        marginLeft: theme.spacing(2),
      },
    },
    "& .showPlayerAnswers__tickIcon": {
      color: theme.palette.success.dark,
    },
    "& .showPlayerAnswers__questionIcon": {
      color: theme.palette.primary.main,
    },
  },
}));

export const ShowPlayerAnswers = () => {
  const classes = useStyles();
  const { state } = useContext(store);
  const { rounds } = state as ShowAnswers;

  const lastRound = rounds[rounds.length - 1];
  const { question, correctAnswer } = lastRound.question;

  return (
    <Card className={classes.showPlayerAnswers}>
      <Box className="showPlayerAnswers__questionAnswer">
        <LiveHelp className="showPlayerAnswers__questionIcon" />
        <Typography variant="h5">{question}</Typography>
      </Box>
      <Box className="showPlayerAnswers__questionAnswer">
        <Beenhere className="showPlayerAnswers__tickIcon" />
        <Typography variant="h5">{correctAnswer}</Typography>
      </Box>
      <List>
        {lastRound.points.map(p => {
          return (
            <PlayerAnswer
              key={p.playerName}
              playerName={p.playerName}
              answer={p.answer}
              correct={p.correct!}
              points={p.points!}
            />
          );
        })}
      </List>
      <Box>
        <Countdown timeLimitMs={5000} />
      </Box>
    </Card>
  );
};
