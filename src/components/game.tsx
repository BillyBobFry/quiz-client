import { Box } from "@material-ui/core";
import React, { useContext } from "react";
import { store } from "../context";
import { ChooseQuestion, ChooseAnswer, Leaderboard } from "./";
import { ShowPlayerAnswers } from "./showPlayerAnswers";
import { makeStyles, Card } from "@material-ui/core";
import { RoundProgress } from "./roundProgress";

const useStyles = makeStyles(theme => ({
  game: {
    "& .game__leaderboard": {
      backgroundColor: "#ffffffdd",
      position: "fixed",
      bottom: 0,
      left: 0,
    },
  },
}));

export const Game = () => {
  const classes = useStyles();
  const { state } = useContext(store);
  const { round } = state;

  return (
    <Box className={classes.game}>
      <RoundProgress />
      {round === "choosing" ? (
        <ChooseQuestion />
      ) : round === "answering" ? (
        <ChooseAnswer />
      ) : round === "show answers" ? (
        <ShowPlayerAnswers />
      ) : round === "game over" ? (
        <Card>
          <Leaderboard />
        </Card>
      ) : (
        <div>Never</div>
      )}
      <Leaderboard className="game__leaderboard" />
    </Box>
  );
};
