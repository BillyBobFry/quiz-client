import React, { useEffect, useState } from "react";
import {
  Box,
  LinearProgress,
  LinearProgressProps,
  makeStyles,
  Typography,
} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  countdown: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-evenly",
    height: "max-contents",
    "& .countdown__progressBar": {
      width: "100%",
    },
    "& .countdown__progressSeconds": {},
  },
}));

export interface CountdownProps extends LinearProgressProps {
  timeLimitMs: number;
}

export const Countdown = (props: CountdownProps) => {
  const classes = useStyles();
  const { timeLimitMs } = props;
  const [timeLeft, setTimeLeft] = useState<number>(0);

  useEffect(() => {
    setTimeout(() => {
      if (timeLeft < 100) setTimeLeft(timeLeft + 1);
    }, timeLimitMs / 100);
  }, [timeLeft, timeLimitMs]);
  return (
    <Box className={classes.countdown}>
      <LinearProgress
        className="countdown__progressBar"
        color="secondary"
        variant="determinate"
        value={timeLeft}
      />
      <Typography className="countdown__progressSeconds">
        {Math.ceil(timeLimitMs / 1000 - (timeLimitMs * timeLeft) / 100000)}
      </Typography>
    </Box>
  );
};
