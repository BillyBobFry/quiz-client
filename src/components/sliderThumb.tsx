import React from "react";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  sliderThumb: {
    border: `2px solid ${theme.palette.secondary.main}`,
  },
}));
export const SliderThumb = (props: {
  children: any;
  open: boolean;
  value: number;
}) => {
  const classes = useStyles();
  return <div className={classes.sliderThumb}>{props.value}</div>;
};
