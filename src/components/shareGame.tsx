import React, { useState } from "react";
import {
  Typography,
  makeStyles,
  Snackbar,
  IconButton,
  Button,
} from "@material-ui/core";
import { Card } from "./";
import { Share, Cancel } from "@material-ui/icons";

const useStyles = makeStyles(theme => ({
  shareGame: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    textTransform: "none",
    "& > :not(:first-child)": {
      marginLeft: theme.spacing(2),
    },
  },
}));

export const ShareGame = () => {
  const classes = useStyles();
  const [snackbarOpen, setSnacbkarOpen] = useState(false);

  const copyToClipboard = () => {
    navigator.clipboard.writeText(window.location.href);
  };
  return (
    <>
      <Card
        title="Game"
        onClick={() => {
          setSnacbkarOpen(true);
        }}
      >
        <Button
          onClick={copyToClipboard}
          fullWidth
          className={classes.shareGame}
        >
          <Share color="primary" />
          <Typography color="primary" variant="h6">
            {window.location.href}
          </Typography>
        </Button>
      </Card>
      <Snackbar
        open={snackbarOpen}
        message="URL copied to clipboard!"
        autoHideDuration={2000}
        onClose={() => {
          setSnacbkarOpen(false);
        }}
        action={
          <IconButton
            onClick={() => {
              setSnacbkarOpen(false);
            }}
          >
            <Cancel color="secondary" />
          </IconButton>
        }
      />
    </>
  );
};
