import { Typography, ListItem, makeStyles } from "@material-ui/core";
import React, { useState, useEffect } from "react";

const useStyles = makeStyles(theme => ({
  playerAnswer: {
    backgroundColor: "white",
    transition: "background-color 0.6s cubic-bezier(.62,.4,.74,1.47)",
    "&.--correct": {
      backgroundColor: theme.palette.success.light,
    },
    "&.--incorrect": {
      backgroundColor: theme.palette.error.light,
    },
  },
}));

export interface PlayerAnswerProps {
  playerName: string;
  answer: string;
  correct: boolean;
  points: number;
}

export const PlayerAnswer = (props: PlayerAnswerProps) => {
  const { playerName, answer, correct, points } = props;
  const [showColours, setShowColours] = useState(false);
  const classes = useStyles();

  useEffect(() => {
    setTimeout(() => {
      setShowColours(true);
    }, 700);
  }, []);
  return (
    <ListItem
      className={[
        classes.playerAnswer,
        !showColours ? "" : correct ? "--correct" : "--incorrect",
      ].join(" ")}
    >
      <Typography>
        {playerName} - {answer} - {points}
      </Typography>
    </ListItem>
  );
};
