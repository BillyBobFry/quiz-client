import { Button, Slider, makeStyles } from "@material-ui/core";
import React, { useContext, useMemo, useState } from "react";
import { Answering, ClosestQuestion, store } from "../context";
import { chooseAnswer } from "../integration";

const useStyles = makeStyles(theme => ({
  chooseClosestAnswer: {
    padding: theme.spacing(2),
    display: "flex",
    alignItems: "center",
    "& > :not(:first-child)": {
      marginLeft: theme.spacing(2),
    },
  },
}));

export const ChooseClosestAnswer = (props: ClosestQuestion) => {
  const { state, ws } = useContext(store);
  const classes = useStyles();
  const { id, currentPlayer } = state as Answering;
  const { round, random } = Math;
  const correctAnswer = Number(props.correctAnswer);
  const third = round(correctAnswer / 3);

  /* eslint-disable react-hooks/exhaustive-deps */
  const min = useMemo(() => round(correctAnswer - random() * third), [
    correctAnswer,
  ]);
  const max = useMemo(() => round(correctAnswer + random() * third), [
    correctAnswer,
  ]);
  /* eslint-enable react-hooks/exhaustive-deps */

  const [answer, setAnswer] = useState(round(min + random() * (max - min)));

  return (
    <div className={classes.chooseClosestAnswer}>
      <Slider
        min={min}
        max={max}
        value={answer}
        onChange={(e, val) => {
          setAnswer(val as number);
        }}
        valueLabelDisplay="on"
        // ValueLabelComponent={SliderThumb}
      />
      <Button
        onClick={() => {
          chooseAnswer(ws!, id, currentPlayer!, String(answer));
        }}
      >
        Submit {answer}
      </Button>
    </div>
  );
};
