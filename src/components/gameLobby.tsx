import React, { useContext } from "react";
import {
  Checkbox,
  Table,
  TableBody,
  TableHead,
  TableRow,
  makeStyles,
  Box,
  TableCell,
  Button,
  lighten,
} from "@material-ui/core";
import { Card, ShareGame, RoundLimit } from "../components";
import { store } from "../context";
import { setReady, startGame } from "../integration";
const useStyles = makeStyles((theme) => ({
  gameLobby: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "100%",
    gap: theme.spacing(5),
    "& > *": {
      width: "100%",
    },

    "& .gameLobby__table": {
      width: `calc(100% - ${theme.spacing(0)}px)`,
      maxWidth: "1000px",
    },

    "& .gameLobby__activePlayer": {
      border: `2px solid ${theme.palette.primary.main}`,
      backgroundColor: lighten(theme.palette.secondary.main, 0.85),
    },

    "& .gameLobby_startButton": {
      maxWidth: "50%",
      padding: theme.spacing(2),
    },
  },
}));

export const GameLobby = () => {
  const classes = useStyles();
  const { state, ws } = useContext(store);
  const { id, players, currentPlayer } = state;

  const toggleReady = () => {
    const currentReady = players.find(
      (player) => player.name === currentPlayer
    )?.ready;
    if (ws && currentPlayer) setReady(ws, id, currentPlayer, !currentReady);
  };

  const allPlayersAreReady = players.every((player) => player.ready);

  const _startGame = () => {
    if (ws) startGame(ws, id);
  };

  return (
    <Box className={classes.gameLobby}>
      <ShareGame />
      <Card title="Players">
        <Table className="gameLobby__table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Ready</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {players.map((player) => {
              const activePlayer = player.name === currentPlayer;
              return (
                <TableRow
                  key={player.name}
                  className={activePlayer ? "gameLobby__activePlayer" : ""}
                >
                  <TableCell>{player.name}</TableCell>
                  <TableCell>
                    <Checkbox
                      disabled={!activePlayer}
                      checked={player.ready}
                      color="primary"
                      onChange={toggleReady}
                    />
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Card>
      <Card title="Options">
        <RoundLimit />
      </Card>
      <Button
        disabled={!allPlayersAreReady}
        variant="contained"
        onClick={_startGame}
        className="gameLobby_startButton"
      >
        Start Game
      </Button>
    </Box>
  );
};
