import { useMemo, useContext } from "react";
import { store } from "../context";

export const usePlayerPoints = () => {
  const { state } = useContext(store);
  const { players, rounds } = state;

  /* eslint-disable react-hooks/exhaustive-deps */
  const points = useMemo(
    () =>
      rounds.reduce(
        (acc, curr) => {
          return acc.map(score => ({
            ...score,
            points:
              score.points +
              (curr.points.find(p => p.playerName === score.name)?.points || 0),
          }));
        },
        players.map(player => ({ name: player.name, points: 0 }))
      ),
    [rounds]
  );
  /* eslint-enable react-hooks/exhaustive-deps */

  const ascendingScore = points.sort((a, b) => (a.points < b.points ? -1 : 1));
  const descendingScore = points.sort((a, b) => (a.points < b.points ? 1 : -1));

  return { points, ascendingScore, descendingScore };
};
